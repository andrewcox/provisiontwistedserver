"""This is a really simple method of creating a twisted server on AWS.
Requires boto and fabric (as per requirements.txt)
Best way to use this is to create a new python script of the following form:

#START SCRIPT
import provision
config = {
    "region" : "us-east-1",
    "image_id" : "ami-51792c38",
    "instance_type" : "t1.micro",
    "security_groups" : ["mailthrottler-1"],
    "key_name" : "mailthrottler",
    "key_location" : "mailthrottler.pem",
    "sc_output_location" : "mailthrottler",
    "sc_command" : "svn checkout http://mailthrottler.googlecode.com/svn/trunk/ mailthrottler",
    "twistd_command" : "twistd -o -y mailThrottler.tac && sleep 5",
    "twistd_proof_of_life" : "tail twistd.pid"
}
provision.launch_and_run(config)
#END SCRIPT

Just fill in the the config values as per your needs.
"""

import boto.ec2
from fabric.api import *
import time
import fabric.network
import os

class Dummy_Status(object):
    status = "unknown"


def getStatus(i_status):
    system_status = getattr(i_status, "system_status", Dummy_Status).status
    instance_status = getattr(i_status, "instance_status", Dummy_Status).status
    return (system_status, instance_status)


def launch_instance(conn, config):
    dummy = Dummy_Status() 
    #assume that we have the security_group setup already??
    #assume that we have the keypair already
    print "Launching instance"
    reservation = conn.run_instances(config["image_id"],
                  max_count = 1,
                  key_name = config["key_name"],
                  instance_type = config["instance_type"],
                  security_groups = config["security_groups"]
                  )

    myinstance = reservation.instances[0]
    print "launched", myinstance.id 
    #wait for state to reach 
    t1 = time.time()
    while myinstance.state != "running":
        print "\tWaiting for instance to reach running state.  Current state:", myinstance.state
        time.sleep(30)
        myinstance.update()
        if time.time() - t1 > (5*60):
            break

    if myinstance.state != "running":
        print "Failed to start instance, please check AWS console"
        return (False, myinstance)
    
    print "\tInstance now running, must wait for it to be reachable"
    time.sleep(10)
    #now we wait till it can be connected to
    i_status = conn.get_all_instance_status(myinstance.id)[0]
    t1 = time.time()
    while (getStatus(i_status) != ("ok", "ok")): 
        print "\tWaiting for instance to be reachable. System:%s Instance:%s" % getStatus(i_status)
        time.sleep(30)
        i_status = conn.get_all_instance_status(myinstance.id)[0]
        if time.time() - t1 > (5*60):
            break
        
    if (getStatus(i_status) != ("ok", "ok")):
        print "Instance not reachable, please check AWS console"
        return (False, myinstance)

    return (True, myinstance)

#assume that you have .boto config setup
def launch_and_run(config):
    print "Connecting to EC2 region:", config["region"]
    conn = boto.ec2.connect_to_region(config["region"])
    launch_success , myinstance = launch_instance(conn,config)
    if not launch_success:
        exit(0)
    public_ip = myinstance.ip_address
    public_dns = myinstance.public_dns_name

    print "public ip:", public_ip
    print "public dns:", public_dns

    env.hosts = [ public_dns ]
    env.host_string = public_dns
    env.user = "ec2-user"
    env.key_filename = config["key_location"]

    sudo("yum install make automake gcc gcc-c++ kernel-devel git-core -y")
    sudo("yum install python27-devel -y")
    if not config.get("svn_not_required"):
        sudo("yum install subversion -y")
    sudo("wget https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py -O - | sudo python27")
    sudo("python27 -m easy_install pip")
    if config.get("ex_commands"):
        for cmd in config["ex_commands"]:
            run(cmd)
    run(config["sc_command"])
    with cd(config["sc_output_location"]):
        sudo("python27 -m pip install -r requirements.txt")
        run(config["twistd_command"])
        run(config["twistd_proof_of_life"])
    fabric.network.disconnect_all()

    print "****INSTANCE CREATED*****"
    print "public ip:", public_ip
    print "public dns:", public_dns
    print "id:",myinstance.id

if __name__ == '__main__':
    print __doc__











